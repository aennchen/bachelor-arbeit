%!TEX root = ../thesis.tex

\chapter{Einführung} \label{chap:Einleitung}

Startups gewinnen in Deutschland mehr und mehr an Bedeutung. Auch wenn sie einen wesentlichen Wirtschaftsfaktor darstellen, ist die zahlenmäßige Erfassung noch nicht weit fortgeschritten \citep{Ripsas2013}. Dank des demographischen Wandels und der resultierenden Bevölkerungsalterung stellt der \gls{AAL}-Bereich mit den \gls{AAL}-Systemen, die Hilfsbedürftigen und Senioren bei einem selbstbestimmten Leben im Alltag unterstützen \citep{Georgieff2008}, viel potentiellen Platz für Startup Unternehmen. Der Senioren- und Pflegemarkt boomt. Im Jahr 2008 waren 20\% der Bevölkerung 65 Jahre und älter (vgl. \autoref{ein:bevoelkerung}). Hochrechnungen des statistischen Bundesamtes zufolge wird diese Bevölkerungsgruppe im Jahr 2060 34\% der Gesamtbevölkerung Deutschlands ausmachen, was 22 Millionen Menschen entspricht. Die Gruppe der über 80-jährigen wird in diesem Zeitraum von etwa 5\% der Bevölkerung auf 14\%, also 9 Millionen Menschen anwachsen \citep{Stat2009}. Das bedeutet, dass in ca. 50 Jahren etwa jeder Siebente in Deutschland 80 Jahre oder älter sein wird. 
Diese Veränderung bedeutet auch, dass immer mehr Senioren und Seniorinnen (65 Jahre und älter) der Bevölkerung im erwerbsfähigen Alter (20 Jahre und älter) gegenüber stehen werden. 

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.75]{graphics/01_Bevoelkerung.pdf}
    \caption[Bevölkerung nach Altersgruppen]{\label{ein:bevoelkerung}Die
deutsche Bevölkerung nach Altersgruppen in den Jahren 2008 und 2060
\citep{Stat2009}.}
\end{figure}

Die immer älter werdende Bevölkerung und der Wunsch der Senioren möglichst lange in den eigenen vier Wänden wohnen und leben zu wollen, auch mit gesundheitlichen Beeinträchtigungen \citep{Georgieff2008}, führt dazu, dass die Aktivitätskontrolle älterer Menschen immer wichtiger wird. Für allein lebende Seniorinnen und Senioren, die viel Zeit unbetreut verbringen, stellt ein Sturz in der eigenen Wohnung eine sehr hohe Gefahr dar. Oftmals sind gestürzte Personen einer hilflosen Lage ausgesetzt, da sie keinen Notruf mehr rufen können. Um diesem Szenario vorzubeugen, gibt es bereits diverse Wohlauf- und Hausnotrufsysteme. Diese etablierten Hilfsmittel unterstützen die Seniorinnen und Senioren und werden hauptsächlich in Betreutem Wohnen oder in der eigenen Wohnung eingesetzt. In diesen Wohnformen wohnen die älteren Personen weitestgehend eigenständig, führen den Haushalt selbständig und können auf Wunsch Unterstützung bekommen.
Beide genannten Hilfsmitteltechiken müssen vom Bewohner aktiv ausgelöst werden. Unregelmäßigkeiten oder fehlende Aktivität der Personen können so im Zweifel erst spät erkannt werden. \\
Verschiedene Monitoringsysteme zur Aktivitätsüberwachung in Seniorenwohnungen mit diversen Sensoren können Lebensgewohnheiten und Bewegungen wahrnehmen. Durch intelligente Analyseverfahren werden die Informationen verarbeitet und Unregelmäßigkeiten oder ein positives Ergebnis erkannt. Bei Unregelmäßigkeiten oder einem Notfall werden Pfleger und/oder Angehörige und die Familie automatisch informiert. So können die Seniorinnen und Senioren die Gewissheit haben, dass sie Hilfe bekommen, auch wenn sie selbst keine rufen können. Außerdem haben, die im Notfall alarmierten Personen die Gewissheit, dass es ihren Verwandten oder Bekannten gut geht, auch wenn sie selbst nicht vor Ort sein können. \gls{AAL}-Systeme beziehen dabei oft bewusst sowohl mögliche Pfleger, als auch die Angehörigen mit in die Unterstützung ein, um so eine bessere Kommunikation zwischen den verschiedenen Parteien zu ermöglichen \citep{Georgieff2008}.

Unter Beachtung oben genannter Faktoren wird nachfolgend auf die konkreten Gründe eingegangen, die als Motivation dieser Arbeit dienen.


\section{Motivation}

Im \gls{AAL}-Bereich werden Monitoringsysteme in Zukunft eine immer größere Rolle spielen, nicht zuletzt durch den vermehrten Einsatz von Automatisierungstechniken in Wohnungen \citep{Georgieff2008} und das Wachstum des Pflegemarktes \citep{Schulz2008}. Aufgrund der relativen Neuheit von Monitoringsystemen in diesem Bereich versuchen sich dort Startups am Markt zu etablieren. 
Für jedes Unternehmen ist ein ausgereiftes Vertriebskonzept unabdingbar. Für Startups ist der Vertrieb und ein funktionierendes Konzept darüber entscheidend, ob sich das Unternehmen weiterhin entwickeln kann oder nicht. Zahlreiche junge Unternehmen scheitern, da zu spät der Fokus auf den Verkauf und das Marketing gesetzt wird \citep{Jacobi2011}. Neben anderen wichtigen Prozessen wie Produktentwicklung, Herstellung und Finanzierung ist der Vertrieb ein weiterer Kernprozess im Unternehmen. \\
Der Pflegebereich ist hinsichtlich der Einführung neuer Techniken sehr träge. Produkte verkaufen sich auf keinen Fall \glqq von alleine\grqq. Startups im Bereich der häuslichen Monitoring Systemen haben es folglich schwer, sich mit äußerst fortschrittlichen Systemen am Markt zu etablieren. In der Pflege werden Neuerungen oftmals erst eingeführt, wenn sie bereits Stand der Technik sind. Der Bereich der Pflege ist in vielerlei Hinsicht rückschrittlich und in der Einführung neuer Techniken sehr langsam. Daher haben Startups, die sich auf dem Pflegemarkt etablieren wollen zum einen die Problematik der Trägheit von Innovationseinführungen in der Pflege und zum anderen die generellen Schwierigkeiten sich an einem Markt zu etablieren. Die Markteinführung eines Produktes in neuen Branchen gestaltet sich generell schwer. Denn obwohl Innovationen den potentiellen Kunden einen Mehrwert schaffen, kommt es zunächst zu Misstrauen der neuen Idee gegenüber. Ein ausgereiftes Vertriebskonzept mit gut ausgewählten Kundensegmenten, realistischen Zielen, einer umsetzbaren Strategie, einer durchdachten Budgetierung, detaillierte Pläne und genauen Kontrollmaßnahmen, stellt die Basis zum erfolgreichen Verkauf des Produktes und somit zur Umsatzgenerierung dar. Vermarktungserfolg stellt gegenüber Wettbewerbern einen Vorsprung dar, der lediglich durch einen Produktvorsprung nicht mehr erreicht werden kann \citep{Detroy2007}. 

Das Ziel dieser Arbeit ist das Erstellen eines Vertriebskonzeptes für Startups, zugeschnitten auf den Bereich der Monitoring Systeme für den häuslichen Gebrauch.

%Zahlen über Startups in Karlsruhe, wie viele Gründungen scheitern?


\section{Aufgabenstellung und Aufbau der Arbeit}

Im Rahmen dieser Bachelorthesis werden zunächst die für die Bearbeitung des Vertriebskonzeptes nötigen Grundlagen erläutert, um dann eine Analyse der verschiedenen Ansätze zum Vertriebskonzept durchzuführen. Insbesondere ist darauf zu achten ein Konzept zur allgemeinen und abstrakten Anwendung zu finden, welches für nicht etablierte Unternehmen anpassbar ist. \\
Die nötigen Modelle und analytischen Methoden zur Unterstützung des Konzeptes werden beschrieben, um in späteren Teilen der Arbeit darauf verweisen zu können. \\
Im Anschluss daran wird das geeignetste Konzept für den Vertrieb herausgearbeitet und am Beispiel eines Startup Unternehmens angewendet. Schritt für Schritt wird das Vertriebskonzept durchgeführt und aufgebaut. Der Fokus soll dabei auf den Besonderheiten liegen, die bei der Anwendung eines solchen Konzeptes auf junge Unternehmen auftreten. Diese werden herausgearbeitet und zusammengefügt, um daraus ein allgemein gültiges Vertriebskonzept für Startups im Bereich der häuslichen Monitoring Systeme zu erhalten. Um eine spätere Verwendung des Vertriebskonzeptes zu gewährleisten, soll dies als Leitfaden für Startups dienen. \\
Im Anschluss an den allgemeinen Transfer des Vertriebskonzeptes wird eine Zusammenfassung über die gesamte Bachelorthesis geschrieben, um dann mit einem Ausblick abzuschließen. 


%\section{Aufbau der Arbeit}

\section{Personenbezeichnungen}

Werden Personenbezeichnungen aus Gründen der besseren Lesbarkeit lediglich in der männlichen oder weiblichen Form verwendet, so schließt dies das jeweils andere Geschlecht mit ein.



