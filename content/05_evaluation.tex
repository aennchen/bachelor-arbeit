%!TEX root = ../thesis.tex
\chapter{Modellierung eines adaptierten Vertriebskonzeptes} \label{chap:Adaptiert}

In diesem Kapitel soll das in \autoref{chap:AnwBsp} angewendete Beispiel eines Vertriebskonzeptes auf das Startup easierLife nun in einen allgemeinen Kontext übertragen werden. Im Ergebnis wird ein Leitfaden zur Erstellung eines Vertriebskonzeptes für nicht etablierte Unternehmen im Bereich der häuslichen Monitoring Systeme entwickelt. Dabei werden die sechs Schritte zum Vertriebskonzept von Detroy, Behle und vom Hofe, die in \autoref{sec:Vertriebskonzept} erläutert und in \autoref{sec:Konzept} angewendet wurden, nochmals nacheinander betrachtet und analysiert. Zum Ende eines jeden Unterkapitels werden die wichtigsten Aspekte für jeden Schritt des Leitfadens zusammengefasst.


\section{Analyse Ist-Zustand}

Bei der Analyse des Ist- Zustandes wurde zunächst die \textbf{SWOT-Analyse} (\autoref{subsec:SWOT1}) durchgeführt. Da diese Analyse alle internen und externen Faktoren eines Unternehmens betrachtet, eignet es sich gut, um ein Grundverständnis über das Unternehmen zu erhalten. Nebenbei ist sie die am häufigsten angewandte Analysemethode zur übersichtlichen Darstellung der Marktposition eines Unternehmens. Für die nachfolgenden Schritte ist es dann aber wichtig, nur die Faktoren, die den Vertrieb betreffen, zu betrachten. Auch die Strategien, die in der Regel nach der Durchführung der Stärken-, Schwächen-, Chancen- und Risikoanalyse aufgestellt werden \citep{Schmitz2012} spielen nur eine untergeordnete Rolle, da in späteren Schritten die Strategien einen gesonderten Punkt darstellen und dort auf den Vertrieb zugeschnitten werden. \\

Zudem wurde das \textbf{Business Model Canvas} wie in \autoref{subsec:BMC} beschrieben, auf das Unternehmen angewandt. Auch wenn dieses Modell nicht zu den Standardansätzen zur Erstellung eines Vertriebskonzeptes gehört, so stellt es doch eine sehr gute Ergänzung im Bezug auf Startups dar und sollte daher unbedingt in den ersten Schritt zur Erstellung eines Vertriebskonzeptes bei Startups aufgenommen werden. Das Modell erleichtert es, den gesamten Prozess der Wertgenerierung zu verstehen und erlaubt es verschiedene Ideen am Beispiel durchzuführen \citep{Osterwalder2010}. Da Startups meist noch kein festgelegtes Kundensegment haben, und es beim genauen Zielsegment im Laufe der ersten Monate auch noch zu Änderungen kommen kann, ist dieser Baustein des Business Model Canvas für nicht etablierte Unternemen sehr wichtig und sollte mehrmals in kurzen Zeitabständen angewendet werden. Ein einmaliges Ausführen und Durchspielen von Ideen, die auf das Unternehmen und den Vertrieb bezogen sind, reicht nicht aus. Auch die Benefits, welche zunächst für ein Kundensegment und die Nutzer plausibel waren, können nach genauerer Betrachtung und Testen in der Praxis kein Wertversprechen mehr darstellen. Dies ist somit auch häufiger zu prüfen. \\

\fbox{\parbox[c][][c]{\textwidth}{\textbf{Zusammenfassung 1. Schritt}\\
\begin{itemize}
\item Analyse des Ist-Zustandes des Startups anhand der SWOT-Analyse, mit besonderem Fokus auf den Faktoren des Vertriebs. Strategien als Ergebnis der Analyse sind an dieser Stelle noch nicht wichtig.
\item Im Anschluss daran, ist die Analyse des Ist-Zustandes anhand des Business Model Canvas mit besonderem Fokus auf dem Kundensegment und den Wertversprechen durchzuführen. Mehrmaliges Ausführen dieses Schrittes nach dem Testen eines Szenarios in der Praxis. 
\end{itemize}
}}


\section{Vertriebsziele}

Die Vertriebsziele eines Startups sollen sich genau wie bei jedem bereits etablierten Unternehmen aus den Unternehmens- oder Marketingzielen, der SWOT-Analyse, sowie im Falle eines Startups auch aus dem Business Model Canvas, ableiten. Die Formulierung quantitativer, also messbarer Ziele ist bei Startups meist nicht einfach. Das Unternehmen verfügt nicht über ausreichend Daten aus vorherigen Jahren, aus denen Trends ablesbar und Ziele zu formulieren wären. Um dieses Problem zu umgehen wird alternativ der Produktlebenszyklus und die Markteinführung herangezogen. Daraus ergeben sich verschiedene Phasen, nach denen, wie in \autoref{ein:Vertriebsziele} graphisch dargestellt, quantitative Ziele eingeteilt werden können. Es ist definiert, in welcher Phase welcher Aspekt des Vertriebes besondere Beachtung erfordert. \\

\begin{figure}[!htbp]{
  \centering
  \includegraphics[scale=0.40]{graphics/09_Vertriebsziele.png}
    \caption[Produktlebenszyklus schematisch]{Schematischer Produktlebenszyklus zur Vertriebszieldefinition}\label{ein:Vertriebsziele}
  } \citep{Schawel2012}
\end{figure}

Allen Phasen voran steht die Produktentwicklung. Sie kann als Phase Null betrachtet werden.\\
Die Produktpilot Phase stellt die erste, für ein Vertriebskonzept eines Startups erforderliche, Phase dar. Sie sollte in der Regel zwischen einem halben und einem Jahr dauern. Einige Systeme sollen in Test-Haushalten angebracht werden, um auf Grund von Feedback der Testnutzer, Verbesserungen am System vornehmen zu können. Außerdem sollen die Zahlungsbereitschaft validiert werden und das Marktpotential geschätzt werden. Der Vertrieb und das Marketing spielen noch keine bedeutende Rolle, denn der Fokus liegt auf dem Produkt und der Weiterentwicklung dessen. \\
Der tatsächliche Markteinstieg ist die zweite Phase. Dabei soll das Produkt von den ersten Kunden gekauft und genutzt werden. Das Produkt rückt in den Hintergrund und der Vertrieb wird sehr wichtig, darauf sollten sich dann die Ressourcenaufwendungen im Unternehmen konzentrieren. In dieser Phase soll das Konzept rund um das Produkt und den Verkauf getestet werden, dadurch werden Fragen geklärt wie beispielsweise, ob die richtigen Kundensegmente auf den richtigen Kanälen angesprochen werden und ob die Bepreisung in Ordnung ist. \\
Der Einstieg in Wachstum stellt die dritte Phase der Markteinführung eines Systems von Startups dar. Vertrieb und Marketing stellen die beiden Hauptaufwendungen des Unternehmens dar. Das Produkt soll nun in größeren Stückzahlen verkauft werden. \\
In der vierten Phase, der Reifephase des Wachstums, in welcher ein Startup nach einem Zeitraum von insgesamt drei bis fünf Jahren ankommen sollte, gilt es das Produkt in hohen Stückzahlen zu verkaufen und sich so Marktanteile zu erkämpfen. Das Unternehmen soll anfangen sich selbst zu tragen und in einem nächsten Schritt, Gewinn abwerfen. \\
Die Phasen könnten nun anhand des Produktlebenszyklusses weiter fortgesetzt werden. Im Bezug auf Startups ist ein Zeitraum von bis zu fünf Jahren schon sehr lang und reicht vollkommen aus, um langfristige Vertriebsziele zu formulieren. \\

Die qualitativen Ziele stellen, aufgrund der schwereren Validierbarkeit, Unterziele der quantitativen Oberziele dar und sind folglich auf dem Weg zum Erreichen der Oberziele entscheidend und sollten daher trotzdem formuliert und eingehalten werden. \\


\fbox{\parbox[c][][c]{\textwidth}{\textbf{Zusammenfassung 2. Schritt}\\
\begin{itemize}
\item Quantitative Ziele anhand des Produktlebenszyklusses und der Markteinführung festlegen. Der Zeitraum von drei bis fünf Jahren kann als langfristig angesehen werden.
\item Qualitative Ziele stellen Unterziele der quantitativen Ziele dar.
\end{itemize}
}}


\section{Vertriebsstrategien}

Um die Vertriebsstrategie zu formulieren gilt es, das richtige Produkt aus dem Produktportfolio zu selektieren. Es gilt zu klären, welches Produkt aus dem Sortiment an welches Kundensegment verkauft werden soll. Die Kundensegmente müssen ausreichend groß sein, sodass irgendwann mit diesem Segment Gewinn erwirtschaftet werden kann \citep{Detroy2007}. \\
Außerdem müssen die geografischen Märkte für jedes Produkt festgelegt werden. \\
Weiterhin ist festzulegen, wie die Art der Marktbearbeitung aussehen soll, also welche Strategie im Vergleich zu Konkurrenten und gegenüber den Kunden gewählt werden soll. Dabei ist es äußerst sinnvoll eine Wettbewerberanalyse durchzuführen, um das eigene Produkt bestmöglich platzieren zu können. Um die Ergebnisse zu veranschaulichen ist es sinnvoll die Wettbewerber und das eigene Unternehmen in einem Positionierungskreuz graphisch darzustellen. Des Weiteres ist eine Positionierung des Angebots vorzunehmen, um so die Positionierungsinhalte gegenüber dem Kunden herauszustellen. Dabei wird definiert, für wen, welches Produkt, durch welches Versprechen, aus welchen Gründen, das perfekte Produkt ist \citep{Detroy2007}. \\
Die Strategie umfasst auch die Überlegungen zu den richtigen Vertriebskanälen für jedes Produkt aus dem Sortiment, dabei wird zwischen direktem und indirektem Vertrieb unterschieden \citep{Detroy2007}. \\
Dieser Schritt weist keine Besonderheiten auf, die nur für Startups gelten. Für diesen Schritt kann das Vertriebsmodell wie von Detroy, Behle und vom Hofe beschrieben ist angewendet werden. \\

\fbox{\parbox[c][][c]{\textwidth}{\textbf{Zusammenfassung 3. Schritt}\\
\begin{itemize}
\item Definition, um welches Produkt es sich handelt, mit der Definition der zugehörigen Kundensegmente und der Definition der geografischen Märkte, in denen das Produkt vertrieben werden soll.
\item Die Strategie im Bezug auf Wettbewerber ist aufgrund einer Wettbewerberanalyse und des Positionierungskreuzes zu definieren.
\item Positionierung des Angebots soll formuliert werden, um zu verdeutlichen, welchen Nutzen welche Kunden von welchem Produkt aufgrund welcher Eigenschaften haben.
\item Untersuchung der Vertriebskanäle mit Unterscheidung zwischen direktem und indirektem Vertrieb.
\end{itemize}
}}


\section{Budgetplanung}

Da Startups kaum großen Umsatz haben und sich auch noch nicht im Sinne der Budgetierung an den Wettbewerbern orientieren können, bleiben für die Budgetplanung nur die zur Verfügung stehenden finanziellen Mittel. Aus diesem Grund ist es sinnvoll den Schritt der Budgetplanung vor der Definition der Vertriebspläne durchzuführen. So wird deutlich, welche Maßnahmen mit dem zur Verfügung stehenden Budget denkbar und umsetzbar sind. \\
Die Budgetplanung bei Startups weist generell einige Besonderheiten gegenüber der Budgetplanung bei bereits fest etablierten Unternehmen auf, da Startups meist nur ein sehr kleines Budget durch Investoren oder Stipendien zur Verfügung haben. Bedingt durch die Geldgeber ist es meist der Fall, dass die Posten für das Budget fest vorgegeben sind und oftmals anderweitig abgerechnet werden. Es gilt, die für den Vertrieb bestimmten finanziellen Mittel bestmöglich zu verwenden. 
Nach der Ziel-Maßnahmen-Kalkulation soll die Budgetierung so durchgefürht werden, dass in möglichst kleinen Abständen ein Soll-/Ist-Vergleich durchgeführt werden kann um bei Fehlentwicklungen schnell aufmerksam zu werden und gegenwirken zu können. \\
Ein weiterer Punkt, den es bei Startups zu beachten gilt, ist, dass eine langfristige Planung ebenfalls meist nicht möglich ist. Oftmals ist unklar, was nach dem ersten Zeitraum des Stipendiums oder der Investition passiert. Somit ist das Budget meist nur mittelfristig zu verplanen, bis dem Startup eine neue Form der Förderung zur Verfügung steht. \\
Generell sollen 10\% des Budgets zurück behalten werden um einen Puffer für Fehlplanungen zu haben. \\

\fbox{\parbox[c][][c]{\textwidth}{\textbf{Zusammenfassung 4. Schritt}\\
\begin{itemize}
\item Finanzielle Mittel für den Vertrieb sind bestmöglich einzuplanen.
\item Soll-/Ist-Abgleiche sind regelmäßig durchzuführen.
\item Meist ist nur eine mittelfristige Budgetplanung möglich.
\item 10\% des Gesamtbudgets sollen als Reserve zurüchgehalten werden.
\end{itemize}
}}


\section{Vertriebspläne}

Wenn der Schritt der Vertriebspläne auf ein Startup angewendet werden soll, ist Folgendes zu tun und zu beachten: \\
Zunächst sind die Ressourcen für jedes Produkt zu planen, also zu definieren, welcher Mitarbeiter sich an welchem Markt mit welchem Produkt beschäftigen soll, um bis zu welchem Datum welches Ziel erreichen zu können. Die Ziele können an dieser Stelle mit dem Umsatz definiert werden \citep{Detroy2007}. Anders als bei etablierten Unternehmen können junge Startups keinen zu erwartenden Umsatz aus Vorjahreszahlen definieren, sondern sind mittelfristig, mit dem Vertriebsziel abgestimmt, festzulegen. \\
Danach sind die Vertriebssysteme im Detail zu gestalten. Dabei geht es bei Startups zunächst um die Selektion der Vertriebssysteme, welches in den meisten Fällen, zu Beginn eines Startups, nur der direkte Verkauf ist. Erst in späteren Phasen wird der indirekte Verkauf anfangen eine Rolle zu spielen. Dabei ist zu wählen, welchen Grad der Distribution die Absatzmittler und Multiplikatoren einnehmen sollen. \\
Die Veränderung der Infrastruktur in einem Startup soll mit der Zeit passieren und sich an den Phasen der Markteinführung orientieren. In Anfangsphasen 0 und I, der Entwicklung und dem Produktpilot, soll die Infrastruktur auf die Entwicklung und Verbesserung des Produktes ausgelegt werden, in Phase II, dem Markteinstieg, muss die Infrastruktur verändert oder ausgeweitet werden, um den neuen Aufgaben des Vertriebs gerecht zu werden. Somit unterscheidet sich dieser Punkt von der Vorgehensweise bei etablierten Unternehmen. Hier wird die Infrastruktur nur verändert, wenn sie nicht mehr effizient ist, nicht aber weil es die Phase, in der sich das Unternehmen mit den Produkten befindet, es verlangt. \\

\fbox{\parbox[c][][c]{\textwidth}{\textbf{Zusammenfassung 5. Schritt}\\
\begin{itemize}
\item Die Ressourcen sind so zu planen, dass bis zu einem festgelegten Datum ein geplanter Umsatz erreicht werden kann. 
\item Die Gestaltung der Vertriebssysteme ist durchzuführen. 
\item Veränderung der Infrastruktur über die Zeit.
\end{itemize}
}}


\section{Kontrollmaßnahmen}

Die Definition der Kontrollmaßnahmen bei Startups gestaltet sich genau wie bei etablierten Unternehmen. Objekte, Verfahren und Termine sollen bei der Überprüfung helfen, ob sich das Unternehmen auf dem Weg, die Ziele zu erreichen, im Sollbereich aufhält oder ob es zu Abweichungen gekommen ist. Falls Abweichungen registriert werden, gilt es diese sobald sie erkannt wurden zu beheben und zu korrigieren. Kontrollziele müssen mit den Vertriebszielen übereinstimmen und dürfen sich nicht widersprechen. Das Kontrollziel ist mit einem Instrument zur Kontrolle von einem zugeteilten Mitarbeiter bis zu einem festgelegten Termin in regelmaßigen Zeitintervallen zu überprüfen \citep{Detroy2007}. \\

\fbox{\parbox[c][][c]{\textwidth}{\textbf{Zusammenfassung 6. Schritt}\\
\begin{itemize}
\item Kontrollziele sind festzulegen.
\item Es ist zu definieren, mit welchem Kontrollinstrument das Ziel überprüft werden soll.
\item Dies ist von einem festgelegten Mitarbeiter bis zu einem bestimmten Termin, in regelmäßigen Zeitintervallen zu veranlassen.
\end{itemize}
}}


\pagebreak \section{Fazit des adaptierten Modells}

Die Modellierung eines adaptierten Vertriebskonzeptes hat gezeigt, dass viele Schritte eines allgemeinen Konzeptes zum Vertriebskonzept ohne Anpassung für Startups anzuwenden sind. In einigen Punkten sind Anpassungen erforderlich, da Startups gegenüber etablierten Unternehmen andere Charaktereigenschaften aufzeigen, denen es gerecht zu werden gilt. \\
Im Ergebnis kam ein für Startups adaptiertes Vertriebskonzept zustande, welches aus einem Anwendungsbeispiel geschlussfolgert wurde. Dieses adaptierte Konzept wurde in Form eines Leitfadens am Ende jedes Schrittes zusammengefasst, um dem eiligen Leser einen Überblick zu bieten.  \\

Das adaptierte Vertriebskonzept kann in einem zusammenfassenden Schaubild (\autoref{ein:Vertriebskonzept}) dargestellt werden. 

\begin{figure}[!htbp]{
  \centering
  \includegraphics[scale=0.50]{graphics/10_Vertriebskonzept.jpg}
    \caption[Adaptiertes Vertriebskonzept]{Das adaptierte Vertriebskonzept für Startups im Bereich der häuslichen Monitoring Systeme}\label{ein:Vertriebskonzept}
  }
\end{figure}


Bei der Durchführung dieses Vertriebskonzeptes durch ein Startup aus dem Bereich der Monitoring Systeme für den häuslichen Gebrauch, ist folgendermaßen vorzugehen: \\

\begin{enumerate}
\item Analyse des Ist-Zustandes
\end{enumerate}
Beim ersten Schritt des Vertriebskonzeptes wird der Ist-Zustand des Startups untersucht. Einzusetzende Analyseverfahren und Modelle sind dabei die SWOT-Analyse, welche in \autoref{subsec:SWOT} beschrieben wurde, und das Business Model Canvas (vlg. \autoref{subsec:Canvas}). Die Schleife verdeutlicht, dass dieser Schritt bei Startups häufiger durchgeführt werden muss, da besonders bei der Findung der Kundensegmente zu Beginn eines Startups noch Veränderungen auftreten können. Alle Folgenden Schritte müssen dann wieder erneut auf eventuelle Veränderungen des ersten angepasst werden.

\begin{enumerate}[resume]
\item Vertriebsziele
\end{enumerate}
Bei diesem Schritt sind quantitative und qualitative Ziele zum Vertrieb zu formulieren. \\

\begin{enumerate}[resume] 
\item Vertriebsstrategie
\end{enumerate}
Die Vertriebsstrategie legt fest, welche Produkte, an welches Zielsegment, über welche Kanäle und auf welchen Märkten verkauft werden sollen. \\

\begin{enumerate}[resume]
\item Budgetplanung
\end{enumerate}
Dieser Schritt wurde vor die Vertriebspläne gestellt. Dabei geht es um die Verplanung des Budgets, welches dem Startup zur Verfügung steht. \\

\begin{enumerate}[resume] 
\item Vertriebspläne
\end{enumerate}
Der fünfte Schritt des Vertriebskonzeptes ist die Gestaltung der Vertriebspläne. Dabei werden Maßnahmen erstellt, die helfen, die strategischen Ziele zu erreichen. \\ 

\begin{enumerate}[resume]
\item Kontrollmaßnahmen
\end{enumerate}
Der sechste und damit letzte Schritt des Vertriebskonzeptes umfasst die Planung der Kontrollmaßnahmen um bei der Verwirklichung des gesamten Konzeptes schnell zu erkennen, falls es zu Fehlentwicklungen kommen sollte. \\

Im folgenden und letzten Kapitel dieser Arbeit werden nochmals die wichtigsten Erkenntnisse der Arbeit zusammengefasst und im Anschluss daran, mit einem Ausblick abgeschlossen.

